from __future__ import division
import re
import copy
import weakref
import collections
import numpy as np
import pyqtgraph as pg
from pyqtgraph import PlotItem, PlotDataItem, FillBetweenItem
try:
    from qtpy import QtCore
except ImportError:
    from PyQt5 import QtCore
import logging

from .const import CHANNEL_RE
from .plotMenu import NDScopePlotMenu
from . import layout

##################################################

CHANNEL_COLORS = {}

##################################################


class NDScopePlot(PlotItem):
    channel_added = QtCore.Signal(str)
    channel_removed = QtCore.Signal(str)
    new_plot_request = QtCore.Signal('PyQt_PyObject')
    remove_plot_request = QtCore.Signal('PyQt_PyObject')

    def __init__(self, *args):
        """Initialize NDSCope Plot object

        """
        super(NDScopePlot, self).__init__(*args)
        # dict of channel:curve
        self.channels = collections.OrderedDict()
        # dict of channel:label
        self.labels = {}
        # list of bad data regions
        self._bad_data_regions = []

        # plot options
        # use automatic downsampling and clipping to reduce the
        # drawing load
        self.setDownsampling(mode='peak')
        # clip data to only what's visible
        # FIXME: is this what we want?
        self.setClipToView(True)
        # don't auto-range x axis, since we have special handling
        self.disableAutoRange(axis='x')
        # hide auto-scale buttons
        self.hideButtons()
        # add legend
        self.addLegend()
        self.legend.setVisible(False)
        # show grid lines
        self.showGrid(x=True, y=True, alpha=0.2)

        self.ctrlMenu = None
        vb = self.getViewBox()
        vb.menu = NDScopePlotMenu(self)

    def add_channel(self, channel, **kwargs):
        """Add channel to plot

        keyword arguments are passed directly to NDScopePlotChannel.

        """
        if channel in self.channels:
            return
        if CHANNEL_RE.match(channel) is None:
            # FIXME: should handle this error better in ancestors
            raise ValueError("invalid channel name: {}".format(channel))
        logging.info("adding channel: {}".format(channel))
        cc = NDScopePlotChannel(channel, **kwargs)
        self.channels[channel] = cc
        self.addItem(cc.curves['y'])
        self.labels[channel] = cc.label
        self._update_title_legend()
        cc.label_changed.connect(self._update_legend_item)
        self.channel_added.emit(channel)

    def remove_channel(self, channel):
        """remove channel from plot

        """
        if channel not in self.channels:
            return
        cc = self.channels[channel]
        self.legend.removeItem(cc.label)
        for curve in cc.curves.values():
            self.removeItem(curve)
        del self.channels[channel]
        self._update_title_legend()
        self.channel_removed.emit(channel)

    # SLOT
    def _update_legend_item(self, channel):
        cc = self.channels[channel]
        label = cc.label
        self.legend.removeItem(self.labels[channel])
        self.legend.addItem(cc.curves['y'], label)
        self.labels[channel] = label

    def _update_title_legend(self):
        """update plot title and legend"""
        if len(self.channels) < 1:
            self.legend.setVisible(False)
            self.setTitle(None)
        elif len(self.channels) == 1:
            self.legend.setVisible(False)
            self.setTitle(list(self.channels.values())[0].label)
        else:
            self.legend.setVisible(True)
            self.setTitle(None)

    def clear_data(self):
        """clear data for all channels

        """
        for curve in self.channels.values():
            curve.clear_data()

    def update(self, data, t0):
        """update all channel

        `data` should be a DataBufferDict object, and `t0` is the GPS
        time for t=0.

        """
        # clear all stored bad data blocks
        for bdr in self._bad_data_regions:
            self.removeItem(bdr)
        bad_data_segs = set()

        ctype_changed = False

        for channel, cc in self.channels.items():
            if channel not in data or not data[channel]:
                continue

            cd = data[channel]

            if cd.is_trend and not cc.is_trend:
                self.addItem(cc.curves['min'])
                self.addItem(cc.curves['max'])
                if cc.curves['fill']:
                    self.addItem(cc.curves['fill'])
            elif not cd.is_trend and cc.is_trend:
                self.removeItem(cc.curves['min'])
                self.removeItem(cc.curves['max'])
                if cc.curves['fill']:
                    self.removeItem(cc.curves['fill'])

            ctype_changed |= cc.set_ctype(cd.ctype)

            bds = cc.set_data(cd, t0)

            # FIXME: we're not actually combining these segments, just
            # excluding exact duplicates.  slightly unaligned segments
            # from multiple channels will just be overlayed
            bad_data_segs |= set(bds)

        for seg in bad_data_segs:
            # FIXME: line_profiler reveals that building these shaded
            # regions at every update is *very* expensive.  We are
            # bypassing this for now by having curve.set_data not
            # return these.  Need to either improve this or figure out
            # better way to indicate bad data regions.
            bdr = pg.LinearRegionItem(
                values=seg,
                brush=(250, 0, 0, 30),
                #pen=pg.mkPen(None),
                movable=False,
            )
            # FIXME, HACK: this is because for some reason the
            # LinearRegionItem init does not accept the 'pen'
            # kwarg, even though the documenatation says it
            # should
            for line in bdr.lines:
                line.setPen(pg.mkPen(None))
            self.addItem(bdr)
            self._bad_data_regions.append(bdr)

        if ctype_changed:
            self._update_title_legend()

##################################################


class NDScopePlotChannel(QtCore.QObject):
    label_changed = QtCore.Signal(str)

    def __init__(self, channel,
                 color=None, width=1,
                 scale=1, offset=0):
        """Initialize channel curve object

        Holds curves for y value, and for trend min/max/fill.

        `color`, `width`, `scale` and `offset` are all applied to the
        channel trace in the plot.  `color` can be a single letter
        color spec ('b', 'r', etc.), an integer, or an [r,g,b] list.
        See the following for more info:

          http://www.pyqtgraph.org/documentation/functions.html#pyqtgraph.mkColor

        """
        super(NDScopePlotChannel, self).__init__()

        global CHANNEL_COLORS

        self.channel = channel
        self.ctype = None
        self.is_trend = None
        self.params = {
            'color': color,
            'width': width,
            'scale': scale,
            'offset': offset,
        }
        self._update_transform()

        if color:
            color = pg.mkColor(color)
        elif channel in CHANNEL_COLORS:
            color = CHANNEL_COLORS[channel]
        else:
            color = pg.mkColor(layout.get_pen_color())
        CHANNEL_COLORS[channel] = color
        self.params['color'] = color

        pen = pg.mkPen(color, width=width)
        mmc = copy.copy(color)
        mmc.setAlpha(100)
        mmpen = pg.mkPen(mmc, style=QtCore.Qt.DashLine)

        self.curves = {}
        self.curves['y'] = PlotDataItem([0, 0], pen=pen, name=self.label)
        self.curves['min'] = PlotDataItem([0, 0], pen=mmpen)
        self.curves['max'] = PlotDataItem([0, 0], pen=mmpen)
        # FIXME: fill is expensive, so we disable it until figure it out
        if False:
            self.curves['fill'] = FillBetweenItem(
                self.curves['min'],
                self.curves['max'],
                brush=mmc
            )
        else:
            self.curves['fill'] = None

    def set_ctype(self, ctype):
        """Update the channel ctype

        Returns True if type changed, False otherwise

        """
        if ctype == self.ctype:
            return False
        self.ctype = ctype
        if ctype in ['s-trend', 'm-trend']:
            self.is_trend = True
        else:
            self.is_trend = False
        self.label_changed.emit(self.channel)
        return True

    @property
    def label(self):
        """label for this channel"""
        label = self.channel
        if self.params['offset'] != 0:
            label += ' {:+g}'.format(self.params['offset'])
        if self.params['scale'] != 1:
            label += ' *{:g}'.format(self.params['scale'])
        if self.is_trend:
            label += ' [{}]'.format(self.ctype)
        return label

    def _update_transform(self):
        """update the transform function"""
        if self.params['scale'] != 1 or self.params['offset'] != 0:
            def transform(data):
                return (data + self.params['offset']) * self.params['scale']
        else:
            def transform(data):
                return data
        self.transform = transform

    def clear_data(self):
        """clear data for curves

        """
        self.curves['y'].setData(np.array([0, 0]))
        self.curves['min'].setData(np.array([0, 0]))
        self.curves['max'].setData(np.array([0, 0]))

    def set_data(self, data, t0):
        """set data for curves

        Data should be DataBuffer object.  Returns list of
        (start, end) bad data segments.

        """
        t = data.tarray - t0

        # iterate over all mods (raw, or mean/min/max) in the DataBuffer
        for mod, y in data.items():
            if mod in ['raw', 'mean']:
                mod = 'y'
            # FIXME: HACK: replace all +-infs with nans.  the infs
            # were causing the following exception in PlotCurveItem
            # when it tried to find the min/max of the array:
            # ValueError: zero-size array to reduction operation minimum which has no identity
            # using nan is not great, since that's also an indicator
            # of a gap, but not sure what else to use.
            np.place(y, np.isinf(y), np.nan)
            self.curves[mod].setData(
                x=t,
                y=self.transform(y),
                connect="finite",
            )

        # look for any bad (nan) data
        # FIXME: bad data handling is very expensive so we disable
        # this for now.  see plot.update() above
        bdsegs = []
        if False:
            bad_data = np.isnan(y)
            if np.any(bad_data):
                nani = np.where(bad_data)[0]
                bi = np.where(np.diff(nani) > 1)[0]
                bstart = np.append(nani[0], nani[bi + 1])
                bend = np.append(nani[bi], nani[-1])
                bdsegs = zip(t[bstart], t[bend])

        return bdsegs
