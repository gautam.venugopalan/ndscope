# HACK: a whole module of stupid old pyqtgraph hacks
import pyqtgraph as pg


mkPen = pg.mkPen
TextItem = pg.TextItem


def InfiniteLine(*args, **kwargs):
    try:
        return pg.InfiniteLine(**kwargs)
    except TypeError:
        label = pg.LabelItem(
            label=kwargs['label'],
            **kwargs['labelOpts']
        )
        del kwargs['label']
        del kwargs['labelOpts']
        il = pg.InfiniteLine(**kwargs)
        label.setParent(il)
        il.label = label
        return il
