import numpy as np
import collections
from gpstime import gpsnow
try:
    from qtpy import QtCore
    from qtpy.QtCore import Signal, Slot
except ImportError:
    from PyQt5 import QtCore
    from PyQt5.QtCore import pyqtSignal as Signal
    from PyQt5.QtCore import pyqtSlot as Slot
import traceback
import logging

from . import const
from . import nds


class DataBuffer(object):
    """data storage

    The data attribute here is actually a dict of sub-data arrays all
    with the same meta-data.  For trend data the keys should be
    ['mean', 'min', 'max'], and for full they would just be ['raw'].

    """

    __slots__ = [
        '__weakref__',
        'channel', 'ctype', 'sample_rate',
        'data', 'size', 'gps_start', 'tarray',
        'lookback', 'lookback_samples_max',
        'last_append_len',
    ]

    def __init__(self, buf, lookback=None):
        """initialize with NDS-like Buffer object"""
        self.channel, mod, self.ctype = nds.parse_channel(buf.channel)
        # HACK: fix m-trend sample rate.  The rate returned from NDS
        # is not accurate, seemingly subject to round-off error:
        # https://git.ligo.org/nds/nds2-distributed-server/issues/1
        # hopefully this should be fixed.  but in general we are
        # subject to round-off accumulation error in here as well (see
        # self.tlen())
        if self.ctype == 'm-trend':
            self.sample_rate = 1.0/60.0
        else:
            self.sample_rate = buf.channel.sample_rate
        self.data = {}
        self.data[mod] = buf.data
        self.gps_start = buf.gps_seconds + buf.gps_nanoseconds*1e-9
        self.update_tarray()
        self.lookback = lookback or self.tlen
        # set maximum lookback time
        self.lookback_samples_max = int(const.DATA_LOOKBACK_LIMIT_BYTES / buf.channel.DataTypeSize() / buf.channel.sample_rate)
        self.last_append_len = 0

    def __repr__(self):
        return "<DataBuffer {} ({}), {} Hz, [{}, {})>".format(
            self.channel, self.ctype, self.sample_rate, self.gps_start, self.gps_end)

    def __len__(self):
        # FIXME: this is a hack way of doing this, and probably
        # doesn't perform well
        return list(self.data.values())[0].size

    def __getitem__(self, mod):
        return self.data[mod]

    def items(self):
        return self.data.items()

    @property
    def is_trend(self):
        return self.ctype in ['s-trend', 'm-trend']

    def set_lookback(self, lookback):
        self.lookback = min(int(np.ceil(lookback)), self.lookback_samples_max)

    @property
    def step(self):
        return 1./self.sample_rate

    @property
    def tlen(self):
        """time length of buffer in seconds"""
        # FIXME: this, and consequently gps_end is subject to
        # round-off accumulation error.  Should have better way to
        # calculate time array and gps_end time.
        return len(self) * self.step

    def update_tarray(self):
        # FIXME: see self.tlen()
        self.tarray = np.arange(len(self)) * self.step + self.gps_start

    @property
    def gps_end(self):
        return self.gps_start + self.tlen

    @property
    def span(self):
        return self.gps_start, self.gps_end

    def extend(self, buf):
        """extend buffer to right"""
        assert buf.channel == self.channel
        assert buf.sample_rate == self.sample_rate, "extend buffer sample rate {} does not match {}".format(buf.sample_rate, self.sample_rate)
        assert buf.gps_start <= self.gps_end, "extend buffer start {} does not match end {}".format(buf.gps_start, self.gps_end)
        ind = np.where(buf.tarray > self.gps_end)[0]
        for mod in self.data:
            self.data[mod] = np.append(self.data[mod], buf.data[mod][ind])
        self.update_tarray()

    def extendleft(self, buf):
        """extend buffer to left"""
        assert buf.channel == self.channel
        assert buf.sample_rate == self.sample_rate, "extendleft buffer sample rate {} does not match {}".format(buf.sample_rate, self.sample_rate)
        assert buf.gps_end >= self.gps_start, "extendleft buffer end {} does not match start {}".format(buf.gps_end, self.gps_start)
        ind = np.where(buf.tarray < self.gps_start)[0]
        for mod in self.data:
            self.data[mod] = np.append(buf.data[mod][ind], self.data[mod])
        self.gps_start = buf.gps_start
        self.update_tarray()

    def append(self, buf):
        """append data to the right, keeping overall time span"""
        assert buf.channel == self.channel
        assert buf.sample_rate == self.sample_rate, "append buffer sample rate {} does not match {}".format(buf.sample_rate, self.sample_rate)
        assert buf.gps_start == self.gps_end, "append buffer start {} does not match end {}".format(buf.gps_start, self.gps_end)
        # shift = data.size
        # nd = np.roll(self.data, -shift)
        # nd[-shift:] = data
        # trick for queue buffer array
        lbs = int(self.lookback * self.sample_rate)
        for mod in self.data:
            self.data[mod] = np.append(self.data[mod], buf.data[mod])[-lbs:]
        self.gps_start = max(
            self.gps_start,
            buf.gps_end - lbs*self.step,
        )
        self.update_tarray()
        self.last_append_len = min(len(buf), len(self)) + 1

    def last_append(self, mod):
        """Return (t, y) data of last append"""
        t = self.tarray[-self.last_append_len:]
        y = self.data[mod][-self.last_append_len:]
        return (t, y)


class DataBufferDict(object):
    """

    Takes NDS-like Buffer list at initialization and organizes the
    included data into a dictionary of DataBuffer objects keyd by
    channel.  various trend channels are kept together in the same
    DataBuffer.

    """
    __slots__ = [
        '__weakref__',
        'buffers', 'gps_start', 'gps_end', 'lookback',
    ]

    def __init__(self, nds_buffers, lookback):
        self.buffers = {}
        # buffer lists should have unique channel,ctype,mod combos
        for buf in nds_buffers:
            db = DataBuffer(buf, lookback+1)
            chan = db.channel
            if chan in self.buffers:
                for m, d in db.data.items():
                    self.buffers[chan].data[m] = d
            else:
                self.buffers[chan] = db

    def __repr__(self):
        return "<DataBufferDict {}>".format(
            list(self.buffers.values()))

    def __getitem__(self, channel):
        return self.buffers[channel]

    def __contains__(self, channel):
        return channel in self.buffers

    def items(self):
        return self.buffers.items()

    def values(self):
        return self.buffers.values()

    @property
    def is_trend(self):
        return list(self.buffers.values())[0] in ['s-trend', 'm-trend']

    @property
    def span(self):
        # FIXME: pulling the span from a random channel is not good,
        # since there's no real guarantee that the channels all share
        # the same span.
        return list(self.buffers.values())[0].span

    def set_lookback(self, lookback):
        for db in self.buffers.values():
            db.set_lookback(lookback + 1)

    def update(self, bufs):
        for chan, buf in bufs.items():
            self.buffers[chan] = buf

    def append(self, bufs):
        for chan, buf in bufs.items():
            self.buffers[chan].append(buf)

    def extendleft(self, bufs):
        for chan, buf in bufs.items():
            self.buffers[chan].extendleft(buf)

    def extend(self, bufs):
        for chan, buf in bufs.items():
            self.buffers[chan].extend(buf)


class DataStore(QtCore.QObject):
    data_retrieve_start = Signal(str)
    new_data = Signal('PyQt_PyObject')
    data_retrieve_done = Signal(str)

    TREND_TYPES = ['raw', 'sec', 'min']

    def __init__(self, lookback=5):
        super(QtCore.QObject, self).__init__()
        self.init = True
        # use a counter to hold references to the channels, so that
        # channels as many channel referneces as needed can be added,
        # while only storing one set of channel data
        self._channels = collections.Counter()
        self._lookback = lookback
        self.threads = {}
        self.reset()

    def __getitem__(self, mod):
        return self.db[mod]

    @property
    def online(self):
        return self.threads.get('online') is not None

    ##########

    def _emit_data(self, trend, online=False):
        if trend is None:
            self.new_data.emit((None, None, online))
        else:
            self.new_data.emit((self.db[trend], trend, online))

    ##########

    def add_channel(self, channel):
        self._channels[channel] += 1
        assert self._channels[channel] >= 0
        # return if we already had reference to this channel
        if self._channels[channel] > 1:
            return
        logging.debug((channel, self._channels[channel]))
        # if initializing don't fill
        if self.init:
            return
        # if online restart
        # FIXME: need to handle other online trend types
        elif self.online:
            self.data_retrieve_done.connect(self.start_online)
            self.terminate()
        # else fill in missing data
        else:
            for trend in self.TREND_TYPES:
                if not self.db[trend]:
                    continue
                self.remote_cmd(
                    trend, 'full',
                    self.db[trend].span,
                    channels=[channel])

    def remove_channel(self, channel):
        self._channels[channel] -= 1
        assert self._channels[channel] >= 0
        # FIXME: remove channel data from self.db
        logging.debug((channel, self._channels[channel]))
        if self.online:
            if len(list(self._channels.elements())) > 0:
                self.data_retrieve_done.connect(self.start_online)
            self.terminate()

    @property
    def channels(self):
        # all channels with more than one referent
        return list(self._channels + collections.Counter())

    ##########

    @property
    def lookback(self):
        return self._lookback

    def set_lookback(self, lookback):
        if self.db['raw']:
            self.db['raw'].set_lookback(lookback)
        self._lookback = lookback

    ##########

    def reset(self):
        logging.debug("DATA RESET")
        self.db = {k: None for k in self.TREND_TYPES}
        self._emit_data(None)

    def start_online(self):
        try:
            self.data_retrieve_done.disconnect(self.start_online)
        except TypeError:
            pass
        logging.debug("DATA ONLINE")
        self.reset()
        self.new_data.connect(self._online_backfill)
        self.remote_cmd('raw', 'online', None)

    def _online_backfill(self):
        self.new_data.disconnect(self._online_backfill)
        if not self.db['raw']:
            return
        start, end = self.db['raw'].span
        start -= self.lookback
        self.remote_cmd('raw', 'extendleft', (start, end))

    def update(self, start_end, trend):
        logging.debug("DATA UPDATE: {} {}".format(trend, start_end))

        # if we're online only accept updates for raw data
        if self.online and trend != 'raw':
            return

        # expand range to ints
        rstart = int(start_end[0])
        rend = int(np.ceil(start_end[1]))

        # add padding
        pad = int((rend - rstart) * const.DATA_SPAN_PADDING)
        rstart -= pad
        rend += pad
        # FIXME: this is to prevent requesting data from the future.
        # The -10 is because the NDS servers don't cover the first few
        # seconds of online data, which should be fixed in the
        # servers.
        rend = min(rend, gpsnow() - 10)
        if rend <= rstart:
            return

        # if the requested trend is empty, just get full span
        if self.db[trend] is None:
            self.remote_cmd(trend, 'full', (rstart, rend))
            return

        # get current start/end times, adjusting inward to make sure
        # we account for non-second aligned data due to 16Hz online
        start, end = self.db[trend].span
        start = int(np.ceil(start))
        end = int(end)

        # if the requested span is fully discontiguous with what we
        # already have set the new span to be full.  only do this if
        # we're not in online mode
        if (rend < start or rstart > end) and not self.online:
            self.remote_cmd(trend, 'full', (rstart, rend))

        # otherwise extend
        else:
            # emit what data we have (in case the caller is requesting
            # a trend change), and will emit more below if it turns
            # out we need to extend the range
            self._emit_data(trend)

            if rstart < start:
                self.remote_cmd(trend, 'extendleft', (rstart, start))
            if rend > end:
                self.remote_cmd(trend, 'extend', (end, rend))

    ##########

    def remote_cmd(self, trend, cmd, start_end=None, channels=None):
        self.init = False

        # this is a thread ID used as a kind of primitive lock.  the
        # ID should be unique enough to block requests from similar
        # trend/action combo.  The exception is online, for which we
        # only do one at a time.
        if cmd == 'online':
            tid = 'online'
        else:
            tid = '{}-{}'.format(trend, cmd)

        if tid in self.threads and self.threads[tid]:
            logging.debug("DATA BUSY: {} {}".format(tid, start_end))
            return
        logging.debug("DATA CMND: {} {}".format(tid, start_end))

        if cmd == 'online':
            desc = 'online '
        else:
            desc = ''
        if trend == 'sec':
            desc += "second trend"
        elif trend == 'min':
            desc += "minute trend"
        elif trend == 'raw':
            desc += "raw"
        self.data_retrieve_start.emit("Retrieving {} data...".format(desc))

        channels = channels or self.channels

        t = nds.NDSThread(tid, trend, cmd, channels, start_end)
        t.new_data.connect(self.remote_recv)
        t.done.connect(self.remote_done)
        t.start()
        self.threads[tid] = t

    @Slot('PyQt_PyObject')
    def remote_recv(self, recv):
        trend = recv[0]
        cmd = recv[1]
        buffers = recv[2]
        logging.log(5, "DATA RECV: {} {}".format(trend, cmd))
        # FIXME: should the NDS object just return this directly?
        dbd = DataBufferDict(buffers, self._lookback)
        if cmd == 'full':
            if not self.db[trend]:
                self.db[trend] = dbd
            else:
                self.db[trend].update(dbd)
        elif cmd == 'online':
            if not self.db[trend]:
                self.db[trend] = dbd
            else:
                self.db[trend].append(dbd)
        elif cmd == 'extendleft':
            try:
                self.db[trend].extendleft(dbd)
            except AssertionError:
                # FIXME: this is a hack to get around the fact that
                # left extension during online (which comes about
                # during zoom out, or during pan/zoom right after
                # stop) will sometimes fail if the left end falls off
                # the lookback while waiting for tid "left" to return.
                # Maybe this is the best thing to do here, but it
                # seems inelegant.  We have no guarantee when the left
                # extend will return, though, and during online
                # appends keep happening, so maybe even if we can be
                # more clever to avoid unnecessary left extend calls
                # that are likely to fail, we probably still want to
                # catch this error during online mode.
                logging.info(traceback.format_exc(0))
        elif cmd == 'extend':
            self.db[trend].extend(dbd)
        self._emit_data(trend, online=cmd=='online')

    @Slot('PyQt_PyObject')
    def remote_done(self, recv):
        tid, error = recv
        logging.debug("DATA DONE: {} {}".format(tid, error))
        self.threads[tid] = None
        if error:
            error = "NDS error: {}".format(error)
            logging.warning(error)
        else:
            error = ''
        # signal only if there are no outstanding threads
        for tid, thread in self.threads.items():
            if thread:
                return
        self.data_retrieve_done.emit(error)

    @property
    def active(self):
        for tid, thread in self.threads.items():
            if thread:
                return True
        return False

    def terminate(self):
        logging.debug("DATA TERMINATE")
        for tid, thread in self.threads.items():
            if thread:
                thread.stop()
                # FIXME: thread terminate is causing problems on SL7
                # thread.terminate()
                # self.notify_done()
