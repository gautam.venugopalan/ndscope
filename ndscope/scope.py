# -*- coding: utf-8 -*-
from __future__ import division
import os
import numpy as np
import pyqtgraph as pg
try:
    from qtpy import QtGui, QtWidgets
    from qtpy.QtWidgets import QStyle
    from qtpy import uic
except ImportError:
    from PyQt5 import QtGui, QtWidgets
    from PyQt5.QtGui import QStyle
    from PyQt5 import uic
import logging

from . import __version__
from . import const
from . import util
from .data import DataStore
from .plot import NDScopePlot
from .trigger import Trigger
from .cursors import Crosshair, TCursors, YCursors

##################################################
# CONFIG

def set_background(color='k'):
    if color == 'w':
        pg.setConfigOption('background', 'w')
        pg.setConfigOption('foreground', 'k')


if os.getenv('ANTIALIAS'):
    pg.setConfigOption('antialias', True)
    print("Anti-aliasing ENABLED")
# pg.setConfigOption('leftButtonPan', False)
# see also ViewBox.setMouseMode(ViewBox.RectMode)
# file:///usr/share/doc/python-pyqtgraph-doc/html/graphicsItems/viewbox.html#pyqtgraph.ViewBox.setMouseMode

##################################################

def _preferred_trend_for_span(span):
    if span > const.TREND_THRESHOLD_M:
        return 'min'
    elif span > const.TREND_THRESHOLD_S:
        return 'sec'
    else:
        return 'raw'

##################################################


Ui_MainWindow, QMainWindow = uic.loadUiType(
    os.path.join(os.path.dirname(__file__), 'scope.ui'))


class NDScope(QMainWindow, Ui_MainWindow):
    def __init__(self, layout):
        """initilize NDScope object

        `layout` should be a list of subplot definitions, each being a
        dictionary matching the keyword arguments to add_plot()
        (e.g. row, col, colspan, channels, etc.).

        """
        super(NDScope, self).__init__(None)
        self.setupUi(self)

        # FIXME: HACK: this is an attempt to bypass the following bug:
        #
        # Traceback (most recent call last):
        #   File "/usr/lib/python3/dist-packages/pyqtgraph/graphicsItems/GraphicsObject.py", line 23, in itemChange
        #     self.parentChanged()
        #   File "/usr/lib/python3/dist-packages/pyqtgraph/graphicsItems/GraphicsItem.py", line 440, in parentChanged
        #     self._updateView()
        #   File "/usr/lib/python3/dist-packages/pyqtgraph/graphicsItems/GraphicsItem.py", line 492, in _updateView
        #     self.viewRangeChanged()
        #   File "/usr/lib/python3/dist-packages/pyqtgraph/graphicsItems/PlotDataItem.py", line 671, in viewRangeChanged
        #     self.updateItems()
        #   File "/usr/lib/python3/dist-packages/pyqtgraph/graphicsItems/PlotDataItem.py", line 483, in updateItems
        #     x,y = self.getData()
        #   File "/usr/lib/python3/dist-packages/pyqtgraph/graphicsItems/PlotDataItem.py", line 561, in getData
        #     if view is None or not view.autoRangeEnabled()[0]:
        # AttributeError: 'GraphicsLayoutWidget' object has no attribute 'autoRangeEnabled'
        def autoRangeEnabled():
            return False, False
        self.graphView.autoRangeEnabled = autoRangeEnabled

        ##########

        self.data = DataStore()
        self.last_fetch_cmd = None

        self.Crosshair = Crosshair()
        self.TCursors = TCursors()
        self.YCursors = YCursors()

        self.plots = []
        rows = 0
        for subplot in layout:
            self.add_plot(**subplot)
            rows = max(rows, subplot['row'])

        # self.referenceTimeLabel = self.graphView.addLabel(
        #     "GPS Time",
        #     size='11pt',
        #     #bold=True,
        #     row=rows+1,
        #     col=0,
        #     colspan=1000,
        # )
        # colspan is an arbitrarily large number just so that the
        # label always spans the full width regardless what it is
        self.referenceTimeLabel.setStyleSheet("""
        font-size: 16px;
        color: #FFFFFF;
        background: #000000;
        padding-bottom: 6;
        """)
        self.set_t0(util.gpstime_parse('now').gps())

        ##########
        # data retrieval

        self.data.data_retrieve_start.connect(self._data_retrieve_start)
        self.data.new_data.connect(self._update_plots)
        self.data.data_retrieve_done.connect(self._data_retrieve_done)

        ##########
        # offline

        self.offlineStartGPS.setValidator(QtGui.QDoubleValidator())
        self.offlineStartGPS.textEdited.connect(self.update_offlineStartGreg)
        self.offlineStartGPS.returnPressed.connect(self.fetch)
        # self.offlineStartGPS.setValidator(QtGui.QIntValidator())
        self.offlineStartGreg.textEdited.connect(self.update_offlineStartGPS)
        self.offlineStartGreg.returnPressed.connect(self.fetch)

        self.offlineEndGPS.setValidator(QtGui.QDoubleValidator())
        self.offlineEndGPS.textEdited.connect(self.update_offlineEndGreg)
        self.offlineEndGPS.returnPressed.connect(self.fetch)
        # self.offlineEndGPS.setValidator(QtGui.QIntValidator())
        self.offlineEndGreg.textEdited.connect(self.update_offlineEndGPS)
        self.offlineEndGreg.returnPressed.connect(self.fetch)
        self.offlineNowButton.clicked.connect(self.set_offlineEnd)

        self.fetchButton.clicked.connect(self.fetch)

        ##########
        # trigger

        self.trigger = Trigger()
        self.trigger.sigLevelChanged.connect(self.update_trigger_level)
        self.triggerLevel.setValidator(QtGui.QDoubleValidator())
        self.triggerLevel.returnPressed.connect(self.set_trigger_level)
        self.triggerResetLevel.clicked.connect(self.reset_trigger_level)
        self.triggerSingle.clicked.connect(self.trigger.set_single)
        self.triggerInvert.clicked.connect(self.trigger.set_invert)
        self.triggerGroup.toggled.connect(self.toggle_trigger)

        ##########
        # cursors

        self.crosshairGroup.toggled.connect(self.toggle_crosshair)

        self.cursorTGroup.toggled.connect(self.toggle_t_cursors)
        self.cursorTReset.clicked.connect(self.TCursors.reset)

        self.cursorYGroup.toggled.connect(self.toggle_y_cursors)
        self.cursorYPlot.currentIndexChanged.connect(self.set_y_cursors_to_selected)
        self.cursorYReset.clicked.connect(self.YCursors.reset)

        ##########

        self.controlBar.hide()
        self.controlExpandButton.setIcon(self.style().standardIcon(QStyle.SP_TitleBarShadeButton))
        self.controlExpandButton.setText('')
        self.controlExpandButton.clicked.connect(self._controlExpand)
        self.controlCollapseButton.setIcon(self.style().standardIcon(QStyle.SP_TitleBarUnshadeButton))
        self.controlCollapseButton.setText('')
        self.controlCollapseButton.clicked.connect(self._controlCollapse)

        self.startstopButton.clicked.connect(self.startstop)
        self.startstopButton2.clicked.connect(self.startstop)
        self.resetSpanButton.clicked.connect(self.reset_span)
        self.resetSpanButton2.clicked.connect(self.reset_span)
        self.resetT0Button.clicked.connect(self.reset_t0)
        self.resetT0Button2.clicked.connect(self.reset_t0)

        self.statusBar.addWidget(QtWidgets.QLabel(
            "ndscope {}    NDS server: {}:{}".format(
                __version__,
                const.HOST,
                const.PORT,
            )))

        ##########

        self._data_retrieve_done(None)

        #self.plot0.sigXRangeChanged.connect(self.update_range)
        self.update_range_proxy = pg.SignalProxy(
            self.plot0.sigXRangeChanged,
            rateLimit=1,
            slot=self.update_range,
        )

    ##########

    def _controlExpand(self):
        self.controlBarSmall.hide()
        self.controlBar.show()

    def _controlCollapse(self):
        self.controlBar.hide()
        self.controlBarSmall.show()

    ##########

    def add_plot(self, channels=None,
                 row=None, col=None, colspan=1,
                 yrange=None):
        """Add plot to the scope

        If provided `channels` should be a list of channel:property
        dicts to add to the plot on initialization.

        """
        logging.info("creating plot ({}, {})...".format(row, col))

        plot = NDScopePlot()
        plot.channel_added.connect(self._channel_added)
        plot.channel_removed.connect(self._channel_removed)
        plot.new_plot_request.connect(self.add_plot)
        plot.remove_plot_request.connect(self.remove_plot)

        # tie all plot x-axes together
        if self.plots:
            plot.setXLink(self.plot0)

        # set y ranges
        if yrange in [None, 'auto']:
            plot.enableAutoRange(axis='y')
        else:
            plot.setYRange(*yrange)

        self.plots.append(plot)

        self.TCursors.add_plot(plot)
        self.cursorYPlot.clear()
        self.cursorYPlot.addItems([str(i) for i, p in enumerate(self.plots)])

        if channels:
            # each channel should be a {name: curve_params} dict
            for chan in channels:
                name, kwargs = list(chan.items())[0]
                kwargs = kwargs or {}
                plot.add_channel(name, **kwargs)

        # FIXME: where to add if row/col not specified?  need some
        # sort of layout policy
        self.graphView.addItem(
            plot,
            row=row, col=col,
            colspan=colspan,
        )

        return plot

    def remove_plot(self, plot):
        """remove plot from layout"""
        if len(self.plots) == 1:
            return
        # first remove all channels
        # make copy of list cause we'll be changing it
        for chan in list(plot.channels.keys()):
            plot.remove_channel(chan)
        self.graphView.removeItem(plot)

    @property
    def plot0(self):
        return self.plots[0]

    def plots4chan(self, channel):
        """Return list of plots displaying channel"""
        plots = []
        for plot in self.plots:
            if channel in plot.channels:
                plots.append(plot)
        return plots

    def _update_triggerSelect(self):
        try:
            self.triggerSelect.currentIndexChanged.disconnect(self.update_trigger_channel)
        except TypeError:
            pass
        self.triggerSelect.clear()
        self.triggerSelect.addItems(self.data.channels)
        self.triggerSelect.currentIndexChanged.connect(self.update_trigger_channel)

    def _channel_added(self, channel):
        self.data.add_channel(str(channel))
        self._update_triggerSelect()

    def _channel_removed(self, channel):
        self.data.remove_channel(str(channel))
        self._update_triggerSelect()

    ##########

    def start(self, window=None):
        """Start online mode

        """
        logging.info('START')
        self.crosshairGroup.setEnabled(False)
        self.toggle_crosshair(checked=False)
        if window:
            self.data.set_lookback(abs(window[0]))
        self.data.start_online()
        if window:
            self.setXRange(*window)
        else:
            # FIXME: how do we not hard code the stride here
            self.setXRange(-self.data.lookback, 0.0625)

    def stop(self, message=None):
        """Stop online mode

        """
        logging.info('STOP')
        self.data.terminate()

    def startstop(self):
        """toggle start/stop"""
        if self.data.active:
            self.stop()
        else:
            self.start()

    def fetch(self, **kwargs):
        """Fetch data offline

        May specify `t0` and `window`, or `start` and `end`.

        """
        if 't0' in kwargs:
            t0 = kwargs['t0']
            window = kwargs['window']
            start = t0 + window[0]
            end = t0 + window[1]
        else:
            if 'start' in kwargs:
                start = kwargs['start']
                end = kwargs['end']
            else:
                start = int(float(self.offlineStartGPS.text()))
                end = int(float(self.offlineEndGPS.text()))
            t0 = end
            window = (start-end, 0)
        self.last_fetch_cmd = kwargs
        logging.info('FETCH {}'.format((start, end)))
        self.triggerGroup.setChecked(False)
        self.data.reset()
        self.set_t0(t0)
        self.setXRange(window[0], window[1])
        self._data_update(start, end)

    def reset_span(self):
        """Reset to last fetch span

        """
        logging.info('RESET')
        for plot in self.plots:
            plot.enableAutoRange(axis='y')
        if self.data.online:
            self.setXRange(-self.data.lookback, 0.0625)
        elif self.last_fetch_cmd:
            self.fetch(**self.last_fetch_cmd)

    def update_range(self):
        """update time range on mouse pan/zoom"""
        self.updateGPS()
        if not self.updateOnRange.isChecked():
            return
        (xmin, xmax), (ymin, ymax) = self.plot0.viewRange()
        start = self.t0 + xmin
        end = self.t0 + xmax
        lookback = np.abs(xmin)
        self.data.set_lookback(lookback)
        self._data_update(start, end)


    def _data_update(self, start, end):
        span = end - start
        if self.data.online:
            trend = 'raw'
        else:
            trend = _preferred_trend_for_span(span)
        self.data.update((start, end), trend)

    # SLOT
    def _data_retrieve_start(self, msg):
        self.statusBar.setStyleSheet("QStatusBar{background:rgba(0,100,0,100);}")
        self.statusBar.showMessage(msg)
        self.startstopButton.setText("stop")
        self.startstopButton.setIcon(self.style().standardIcon(QStyle.SP_MediaStop))
        self.startstopButton2.setText("stop")
        self.startstopButton2.setIcon(self.style().standardIcon(QStyle.SP_MediaStop))
        self.offlineTab.setEnabled(False)
        self.resetT0Button.setEnabled(False)
        self.resetT0Button2.setEnabled(False)

    # SLOT
    def _data_retrieve_done(self, error):
        if error:
            self.statusBar.setStyleSheet("QStatusBar{background:rgba(255,0,0,255);color:black;font-weight:bold;}")
            self.statusBar.showMessage(error)
        else:
            self.statusBar.setStyleSheet("")
            self.statusBar.clearMessage()
        # for plot in self.plots:
        #     plot.disableAutoRange(axis='y')
        self.startstopButton.setText("online")
        self.startstopButton.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
        self.startstopButton2.setText("online")
        self.startstopButton2.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
        self.offlineTab.setEnabled(True)
        self.resetT0Button.setEnabled(True)
        self.resetT0Button2.setEnabled(True)
        self.crosshairGroup.setEnabled(True)
        if self.crosshairGroup.isChecked():
            self.toggle_crosshair(checked=True)

    ##########
    # PLOTTING

    # SLOT
    def _update_plots(self, recv):
        data, trend, online = recv

        logging.log(5, "UPDATE PLOT (online={})".format(online))

        if not data:
            logging.log(5, "CLEAR")
            for plot in self.plots:
                plot.clear_data()
            return

        trigger = None
        if online:
            # if we're online, check for triggers
            if self.trigger.active:
                trigger = self.trigger.check(data)
                if not trigger:
                    return
                self.update_trigger_time(trigger)
                self.set_t0(trigger)

            else:
                self.set_t0(data.span[1])

        else:
            # if we're not online, check that the trend matches the
            # current preferred for the span.  if not, ignore the
            # update under the assumption that more appropriate trend
            # blocks are on the way (or are already being displayed)
            (xmin, xmax), (ymin, ymax) = self.plot0.viewRange()
            span = xmax - xmin
            if trend != _preferred_trend_for_span(span):
                logging.log(5, "DROP")
                return

        for plot in self.plots:
            plot.update(data, self.t0)

        if trigger and self.trigger.single:
            self.stop()

    def update_tlabel(self):
        self.referenceTimeLabel.setText(
            '{} from {} [{:0.4f}]'.format(
                'seconds',
                util.gpstime_str_greg(
                    util.gpstime_parse(self.t0),
                    fmt=const.DATETIME_FMT,
                ),
                self.t0,
            )
        )

    def set_t0(self, t0):
        self.t0 = t0
        self.update_tlabel()
        self.updateGPS()

    def reset_t0(self):
        (xmin, xmax), (ymin, ymax) = self.plot0.viewRange()
        t0 = self.t0 + (xmax+xmin)/2
        xd = np.abs(xmax-xmin)/2
        self.set_t0(t0)
        self.setXRange(-xd, xd)
        self._data_update(t0-xd, t0+xd)

    def setXRange(self, start, end):
        logging.debug('RANGE: {} {}'.format(start, end))
        self.plot0.sigXRangeChanged.disconnect()
        self.plot0.setXRange(start, end, padding=0, update=False)
        self.plot0.sigXRangeChanged.connect(self.update_range)

    def updateGPS(self):
        (xmin, xmax), (ymin, ymax) = self.plot0.viewRange()
        start = self.t0 + xmin
        end = self.t0 + xmax
        self.set_offlineStart(start)
        self.set_offlineEnd(end)

    def set_offlineStart(self, time):
        gt = util.gpstime_parse(time)
        self.offlineStartGPS.setText(util.gpstime_str_gps(gt))
        self.offlineStartGreg.setText(util.gpstime_str_greg(gt))

    def set_offlineEnd(self, time=None):
        if time:
            gt = util.gpstime_parse(time)
        else:
            gt = util.gpstime_parse('now')
        self.offlineEndGPS.setText(util.gpstime_str_gps(gt))
        self.offlineEndGreg.setText(util.gpstime_str_greg(gt))

    def update_offlineStartGPS(self):
        gt = util.gpstime_parse(self.offlineStartGreg.text())
        if not gt:
            return
        self.offlineStartGPS.setText(util.gpstime_str_gps(gt))

    def update_offlineStartGreg(self):
        gt = util.gpstime_parse(self.offlineStartGPS.text())
        if not gt:
            return
        self.offlineStartGreg.setText(util.gpstime_str_greg(gt))

    def update_offlineEndGPS(self):
        gt = util.gpstime_parse(self.offlineEndGreg.text())
        if not gt:
            return
        self.offlineEndGPS.setText(util.gpstime_str_gps(gt))

    def update_offlineEndGreg(self):
        gt = util.gpstime_parse(self.offlineEndGPS.text())
        if not gt:
            return
        self.offlineEndGreg.setText(util.gpstime_str_greg(gt))

    ##########
    # TRIGGER

    def set_trigger(self, channel):
        assert channel in self.data.channels + [None]
        if channel == self.trigger.channel:
            return
        if self.trigger.channel is not None:
            tplot = self.plots4chan(self.trigger.channel)[0]
        else:
            tplot = None
        if channel is not None:
            nplot = self.plots4chan(channel)[0]
        else:
            nplot = None
        self.trigger.channel = channel
        if nplot != tplot:
            if tplot:
                tplot.removeItem(self.trigger.line)
            if nplot:
                nplot.addItem(self.trigger.line, ignoreBounds=True)
                nplot.disableAutoRange(axis='y')
            return True
        else:
            return False

    def update_trigger_level(self):
        self.triggerLevel.setText('{:g}'.format(self.trigger.level))

    def set_trigger_level(self):
        value = float(self.triggerLevel.text())
        self.trigger.set_level(value)

    def reset_trigger_level(self):
        chan = self.trigger.channel
        if self.data['raw'] and chan in self.data['raw']:
            y = self.data['raw'][chan].data['raw']
            yn = y[np.where(np.invert(np.isnan(y)))[0]]
            value = np.mean(yn)
        else:
            value = 0
        self.trigger.set_level(value)

    def update_trigger_time(self, time):
        self.triggerTime.setText('{:14.3f}'.format(time))

    def update_trigger_channel(self):
        chan = str(self.triggerSelect.currentText())
        if self.set_trigger(chan):
            self.reset_trigger_level()
        logging.info("trigger set: {}".format(chan))

    def toggle_trigger(self, checked=None):
        if not self.trigger:
            return
        if checked is None:
            checked = self.triggerGroup.isChecked()
        if checked:
            chan = str(self.triggerSelect.currentText())
            self.set_trigger(chan)
            self.reset_trigger_level()
            logging.info("trigger enabled")
        else:
            self.set_trigger(None)
            logging.info("trigger disabled")

    ##########
    # CURSORS

    def toggle_crosshair(self, checked=None):
        if checked is None:
            checked = self.crosshairGroup.isChecked()
        if checked:
            for plot in self.plots:
                plot.disableAutoRange(axis='y')
            # self.graphView.scene().sigMouseMoved.connect(self.Crosshair.update)
            self.crosshair_proxy = pg.SignalProxy(
                self.graphView.scene().sigMouseMoved,
                rateLimit=60,
                slot=self.update_crosshair)
            logging.info("crosshair enabled")
        else:
            self.Crosshair.set_active_plot(None)
            self.crosshair_proxy = None
            logging.info("crosshair disabled")

    def update_crosshair(self, event):
        # using signal proxy unfortunately turns the original
        # arguments into a tuple pos = event
        pos = event[0]
        for plot in self.plots:
            if plot.sceneBoundingRect().contains(pos):
                self.Crosshair.set_active_plot(plot)
                x = plot.vb.mapSceneToView(pos).x()
                t = self.t0 + x
                y = plot.vb.mapSceneToView(pos).y()
                self.Crosshair.update(x, y, t)
                return

    def toggle_t_cursors(self, checked=None):
        if checked is None:
            checked = self.cursorTGroup.isChecked()
        if checked:
            self.TCursors.enable()
            logging.info("T-cursor enabled")
        else:
            self.TCursors.disable()
            logging.info("T-cursor disabled")

    def get_selected_y_cursors_plot(self):
        ind = self.cursorYPlot.currentText()
        if ind:
            return self.plots[int(ind)]

    def set_y_cursors_to_selected(self):
        if not self.cursorYGroup.isChecked():
            return
        plot = self.get_selected_y_cursors_plot()
        if plot:
            self.YCursors.set_plot(plot)

    def toggle_y_cursors(self, checked=None):
        if checked is None:
            checked = self.cursorYGroup.isChecked()
        if checked:
            self.set_y_cursors_to_selected()
            logging.info("Y-cursor enabled")
        else:
            self.YCursors.set_plot(None)
            logging.info("Y-cursor disabled")
