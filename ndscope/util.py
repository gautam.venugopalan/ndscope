from gpstime import gpstime, GPSTimeException

from . import const


def parse_time_window(window):
    if not window:
        return
    try:
        seconds = float(window)
        window = [-seconds/2, seconds/2]
    except:
        try:
            window = window.strip('[]()').split(',')
            def z(e):
                if not e:
                    return 0
                else:
                    return float(e)
            window = list(map(z, window))
        except:
            pass
    window = list(window)
    window = [min(window), max(window)]
    return window


def gpstime_parse(time):
    try:
        return gpstime.parse(time)
    except GPSTimeException:
        return None
    except ValueError:
        return None


def gpstime_str_gps(gt):
    if gt:
        return str(gt.gps())


def gpstime_str_greg(gt, fmt=const.DATETIME_FMT_OFFLINE):
    if gt:
        return gt.astimezone(const.DATETIME_TZ).strftime(fmt)
